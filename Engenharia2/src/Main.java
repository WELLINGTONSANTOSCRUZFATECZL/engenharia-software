
	class Main {
	  public static void main(String[] args) {
		  
		  ControleBonificacao controle = new ControleBonificacao();
		  
		  Vendedor vendedor1 = new Vendedor();
		  vendedor1.calculaComissao(2500.0);
		  vendedor1.bonificacao(900.0);
		  controle.registra(vendedor1);
		  Gerente gerente1 = new Gerente();
		  gerente1.bonificacao(5000.0);
		  controle.registra(gerente1);
		  Diretor diretor1 = new Diretor();
		  diretor1.bonificacao(7000.0);
		  controle.registra(diretor1);

		  System.out.println("Bonificação mais Comissão Vendedor: " + vendedor1.getBonificacao());
		  System.out.println("Bonificação Gerente: " + gerente1.getBonificacao());
		  System.out.println("Bonificação Diretor: " + diretor1.getBonificacao());
		  
		  System.out.println("Valor total de bonificações: " + controle.getTotalDeBonificacoes());
	  }
	}
	    




